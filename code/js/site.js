﻿function loadJSON(callback, jsonFile) {
    var rawFile = new XMLHttpRequest();
    rawFile.open("GET", 'api/facts/' + jsonFile + '.json', false);
    rawFile.onreadystatechange = function () {
        if (rawFile.readyState === 4) {
            if (rawFile.status === 200 || rawFile.status === 0) {
                var allText = rawFile.responseText;
                callback(allText, jsonFile);
            }
        }
    };
    rawFile.send(null);
}

var processJson = function (response, jsonFile) {
    var actual_JSON = JSON.parse(response);
    document.getElementById("myModalLabel").textContent = jsonFile + ' facts';
    var parentElement = document.getElementById('divListContainer');
    while (parentElement.firstChild) {
        parentElement.removeChild(parentElement.firstChild);
    }
    var temp = document.getElementsByTagName("template")[0];
    for (var i = 0; i < actual_JSON.all.length; i++) {
        var clone = temp.content.cloneNode(true);
        clone.querySelector("strong").textContent = actual_JSON.all[i].text;
        if (actual_JSON.all[i].user) {
            clone.querySelector("small").textContent = actual_JSON.all[i].user.name.first + ' ' + actual_JSON.all[i].user.name.first;
        }
        parentElement.appendChild(clone);
    }
};